package com.netcracker.ec.khudnitsky.lect_03.dao.dto;

public class ClientDTO {

  private int id;
  private String firstName;
  private String lastName;
  private int bonusPoints;
  private String city;
  private String street;

  public ClientDTO() {
  }

  public ClientDTO(String firstName, String lastName, int bonusPoints, String city, String street) {
    this.id = 0;
    this.firstName = firstName;
    this.lastName = lastName;
    this.bonusPoints = bonusPoints;
    this.city = city;
    this.street = street;
  }

  public ClientDTO(int id, String firstName, String lastName, int bonusPoints, String city,
                   String street) {
    this.id = id;

    this.firstName = firstName;
    this.lastName = lastName;
    this.bonusPoints = bonusPoints;
    this.city = city;
    this.street = street;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public int getBonusPoints() {
    return bonusPoints;
  }

  public void setBonusPoints(int bonusPoints) {
    this.bonusPoints = bonusPoints;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  @Override
  public String toString() {
    return "ClientDTO{" +
        "firstName='" + firstName + '\'' +
        ", lastName='" + lastName + '\'' +
        ", bonusPoints=" + bonusPoints +
        ", city='" + city + '\'' +
        ", street='" + street + '\'' +
        '}';
  }
}
