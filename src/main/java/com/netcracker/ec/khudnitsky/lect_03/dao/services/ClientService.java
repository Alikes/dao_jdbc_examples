package com.netcracker.ec.khudnitsky.lect_03.dao.services;

import com.netcracker.ec.khudnitsky.lect_03.dao.converters.ClientConverter;
import com.netcracker.ec.khudnitsky.lect_03.dao.db.ClientDAO;
import com.netcracker.ec.khudnitsky.lect_03.dao.db.ClientDAOImpl;
import com.netcracker.ec.khudnitsky.lect_03.dao.dto.ClientDTO;
import com.netcracker.ec.khudnitsky.lect_03.dao.entities.Client;
import java.util.List;
import java.util.stream.Collectors;

public class ClientService {

  private ClientDAO clientDAO;
  private ClientConverter converter = new ClientConverter();

  public ClientService() {
    clientDAO = new ClientDAOImpl();
  }

  public void addClient(ClientDTO dto) {
    Client client = converter.convert(dto);
    clientDAO.addClient(client);
  }

  public List<ClientDTO> getClients() {
    return clientDAO.getClients()
        .stream()
        .map(client -> converter.convert(client))
        .collect(Collectors.toList());
  }
}
