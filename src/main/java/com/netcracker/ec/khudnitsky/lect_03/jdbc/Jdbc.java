package com.netcracker.ec.khudnitsky.lect_03.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Jdbc {

  public static void main(String[] args) {
    //String url = "jdbc:postgresql://192.168.99.100:32768/lecture_03";
    //String username = "postgres";
    //String password = "postgres";
    String url = "jdbc:mysql://127.0.0.1:3306/lecture_03?verifyServerCertificate=false&useSSL=false&requireSSL=false&useLegacyDatetimeCode=false&amp&serverTimezone=UTC";
    String username = "admin";
    String password = "admin";

    try {

      Connection connection = DriverManager.getConnection(url, username, password);

      initialize(connection);

      Statement statement = connection.createStatement();
      ResultSet resultSet = statement.executeQuery("SELECT * FROM students");

      print(resultSet, 2);

      PreparedStatement preparedStatement = connection
          .prepareStatement("SELECT * FROM students WHERE name = ?");
      preparedStatement.setString(1, "Alexei");
      resultSet = preparedStatement.executeQuery();

      print(resultSet, 2);

      resultSet.close();
      statement.close();
      connection.close();

    } catch (Exception e) {
      e.printStackTrace();
    }

    try {
      Connection connection2 = DatabaseConnector.getConnection();

      Statement statement = connection2.createStatement();
      ResultSet resultSet = statement.executeQuery("SELECT * FROM students");

      print(resultSet, 2);

      resultSet.close();
      statement.close();
      connection2.close();
    } catch (SQLException | ClassNotFoundException e) {
      e.printStackTrace();
    }
  }

  private static void initialize(Connection connection) throws SQLException {
    Statement createTableStatement = connection.createStatement();
    boolean alreadyCreated = createTableStatement.execute("CREATE TABLE IF NOT EXISTS students(" +
        "id serial primary key, " +
        "name varchar(100));");

    //if (alreadyCreated) {
      Statement stmt = connection.createStatement();

      try {
        connection.setAutoCommit(false);
        String sql = "INSERT INTO students " +
            "VALUES ('10000', 'Alexei')";
        stmt.executeUpdate(sql);

        sql = "INSERT INTO students " +
            "VALUES (20000, 'Pavel')";
        stmt.executeUpdate(sql);

        sql = "INSERT INTO students " +
            "VALUES (30000, 'Sasha')";
        stmt.executeUpdate(sql);

        sql = "INSERT INTO students " +
            "VALUES (40000, 'Stas')";
        stmt.executeUpdate(sql);
        connection.commit();
      } catch (Exception e){
        connection.rollback();
      }
    //}
  }

  private static void print(ResultSet resultSet, int columnIndex) throws SQLException {
    System.out.println("===============================");
    while (resultSet.next()) {
      System.out.println(resultSet.getString(columnIndex));
    }
  }
}
